External Entities XML Files plugin
**********************************

Enables to manage entities stored in one or more XML files.

Supported structures:
-a single main file with all entities inside.
-complex directory structure with multiple XML files:
  A directory pattern is defined based on the entity fields to match the XML
  file to which the entity belongs to.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This modules provides a way to use XML files content as Drupal entities. Drupal
sees those entities like regular entities while they are not stored in database
but in XML file. While this approach is slower than storing entities in
database, it can provide several advantages and can solve some specific use
cases.

Please be aware that this module does not support concurrent file editing. It
means that if several applications (Drupal and non-Drupal) open a XML file for
editing concurrently, only the last one saving its changes will be recorded.

[XML file](https://en.wikipedia.org/wiki/XML)
All entities can be stored in a single XML as well as in separated XML. In
order to use multiple XML files, a XML file name pattern must be used. The
patterns allow to use entity field values or parts of those in the file name
generation, making it possible to store one entity by XML file, grouping
entities with common values in a same XML, use specific subdirectories
depending on field values and possibly more. Patterns are specified using curly
braces and support a substring notation just like the PHP substr function:
`{offset[,length]:field_name}`
For example, the file name pattern `{-4:id}/{0,3:id}.xml` used on the entity 
with the identifier "12345678_ABCD" will generate the sub-path "ABCD/123.xml".
Following this file name pattern, the entity "12390000_ABCD" would be stored in
the same XML file while the entity "12301234_EFGH" would be stored in
"EFGH/123.xml" (same file name but in a different directory).

Since complex file name patterns can be used, the system needs a way to locate
the appropriate XML file for a given entity when only its identifier is
provided while other fields are used in the pattern but are not available. In
order to solve that, a dedicated index file is required. It will store the
association between an entity identifier and its corresponding file, one by
line, separated by a tab character. It is only required when complex pattern are
used or when the user wants to explicitely specify a given XML file for a given
entity rather than using the default one provided by the file name or name
pattern setting. Therefore, any entry for a given entity in the index file, once
there, will remain unchanged by the system.

Finally, it is possible to order and limit the number of entity field that will
be recorded in a XML file. You can either pre-create the XML file with only
the column name line or use the setting "List of entity field names to store in
XML files". It may be convenient to restrict the number of field saved,
especially when using the xnttmulti module that can combine fields in an entity
from several external sources other than the XML file.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

 * [External Entities Files Storage Client Base](https://www.drupal.org/project/xnttfiles)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new storage client for external entity
types. Then, when you create a new external entity type, you can select the
"XML files" plugin and have access to settings specific to the new external
entity.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
