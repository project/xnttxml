<?php

namespace Drupal\xnttxml\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\FileClientBase;

/**
 * External entities storage client for XML files.
 *
 * @StorageClient(
 *   id = "xnttxml",
 *   label = @Translation("XML Files"),
 *   description = @Translation("Retrieves and stores records in XML files.")
 * )
 */
class XmlFiles extends FileClientBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = 'XML';
    $this->fileTypePlural = 'XML';
    $this->fileTypeCap = 'XML';
    $this->fileTypeCapPlural = 'XML';
    $this->fileExtension = '.xml';
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = [];
    $idf = $this->getSourceIdFieldName() ?? 0;

    // Extract field values from file path.
    $path_data = [];
    $structure = $this->configuration['structure'];
    if (FALSE !== strpos($structure, '{')) {
      $path_data = $entity = $this->getValuesFromPath($file_path);
    }

    $xml = simplexml_load_file($file_path);
    if (FALSE === $xml) {
      $xml_error = '';
      foreach (libxml_get_errors() as $error) {
        $xml_error .= $error->message . "\n";
      }
      $this->logger->error(
        'Failed to parse XML file "'
        . $file_path
        . '": '
        . $xml_error
      );
    }
    else {
      $xml_array = json_decode(json_encode($xml), TRUE);
      if ($this->configuration['multi_records']) {
        $columns = [];
        // Multiple records, we assume we have an array, get list.
        // @todo Use setting tag name to get the value.
        $xml_array = array_shift($xml_array);
        if (is_array($xml_array)) {
          // Check if we have a single record in the list.
          $keys = array_keys($xml_array);
          if ($keys !== range(0, count($xml_array) - 1)) {
            // Single record in the list.
            $data[$xml_array[$idf]] = $xml_array;
            $columns += array_flip($keys);
          }
          else {
            // Process each entity.
            foreach ($xml_array as $entity) {
              if (is_array($entity) && !empty($entity[$idf])) {
                $data[$entity[$idf]] = $entity + $path_data;
                $columns += array_flip(array_keys($entity));
              }
            }
          }
        }
        $data[''] = array_keys($columns) ?: $idf;
      }
      else {
        // Single record. We assume we have one entity.
        if (is_array($xml_array)) {
          // Set a default id value for invalid mappings.
          $xml_array[$idf] ??= current($xml_array);
          $data[$xml_array[$idf]] = $xml_array + $path_data;
          $data[''] = array_keys($xml_array);
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateRawData(array $entities_data) :string {
    // Don't save columns.
    $columns = array_flip($entities_data['']);
    unset($entities_data['']);
    if ($this->configuration['multi_records']) {
      // Multiple values.
      // @todo Use a setting to define root tag element.
      $xml = new \SimpleXMLElement('<entities/>');
      $store_xml = function ($xml, $name, $value) use (&$store_xml) {
        if (is_array($value)) {
          $child_xml = $xml->addChild($name);
          foreach ($value as $subname => $subval) {
            $store_xml($child_xml, $subname, $subval);
          }
        }
        else {
          $xml->addChild($name, $value);
        }
      };
      foreach ($entities_data as $id => $entity) {
        $entity_xml = $store_xml(
          $xml,
          // @todo Use a setting to define entity tag element.
          'entity',
          array_intersect_key($entity, $columns)
        );
      }
      $xml_raw = $xml->asXML();
    }
    else {
      // Single value.
      $entity = array_shift($entities_data);
      $xml_entity = array_flip(array_intersect_key($entity, $columns));
      // @todo Use a setting to define entity tag element.
      $xml = new \SimpleXMLElement('<entity/>');
      array_walk_recursive($xml_entity, [$xml, 'addChild']);
      $xml_raw = $xml->asXML();
    }
    // Write records.
    return $xml_raw;
  }

}
